import { Component, Input, OnInit } from '@angular/core';
import { CakeService } from '../services/cake/cake.service';
import { Cakes } from '../shared/models/cake';
import {Tag} from '../shared/models/Tag';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {
  @Input()
  cakePageTags?:string[];
  @Input()
  justifyContent:string = 'center'
  tags?:Tag[] =[];

  constructor(private ck:CakeService) { }

  ngOnInit(): void {
    if(!this.cakePageTags)
    this.tags = this.ck.getAllTag();
  }

}
