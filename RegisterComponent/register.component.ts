import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user : any;

  constructor(private service: UserService) { 
    this.user = {userName:'', email:'', password:'', confirmpassword:''};
  }

  ngOnInit(): void {
    console.log("Data Received!!!");
  }
  registerUser(){
    this.service.registerUser(this.user).subscribe();
  }


}


