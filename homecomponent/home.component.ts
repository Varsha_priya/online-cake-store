import { Component, OnInit } from '@angular/core';
import { CakeService } from '../services/cake/cake.service';
import { Cakes } from '../shared/models/cake';
import { StarRatingComponent } from 'ng-starrating';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cakes:Cakes[]=[];

  constructor(private ck:CakeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params =>{
      if(params['searchItem'])
      this.cakes = this.ck.getAll().filter(cake => cake.cakeName.toLowerCase().includes(params['searchItem'].toLowerCase()));
      else if(params['tag'])
      this.cakes = this.ck.getAllCakeByTag(params['tag'])
      else
      this.cakes = this.ck.getAll()
    })
    
  }

}
