import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email : string;
  password : string;

  constructor(private router:Router) {
    this.email = '';
    this.password = '';
  }

  ngOnInit(): void {
  }
  loginSubmit(loginForm:any) : void {
    console.log(loginForm)
    // console.log("User Id : " + this.email);
    // console.log("Password : " + this.password);
  }
  newLogin(){
    console.log("Working");
    //  this.router.navigate(['/test']);
    this.router.navigateByUrl('/register');
   }
   
}
