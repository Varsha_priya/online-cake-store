import { Component, OnInit } from '@angular/core';
import { Cakes } from '../shared/models/cake';
import { CakeService } from '../services/cake/cake.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cakepage',
  templateUrl: './cakepage.component.html',
  styleUrls: ['./cakepage.component.css']
})
export class CakepageComponent implements OnInit {
  cake !: Cakes;

  constructor(private activatedRoute:ActivatedRoute,
    private cakeService:CakeService, private cartService:CartService,
    private router:Router) {
      activatedRoute.params.subscribe((params)=>{
        if(params['id'])
        this.cake = cakeService.getCakeById(params['id'])
      })
     }

  ngOnInit(): void {
  }
  addToCart(){
    this.cartService.addToCart(this.cake);
    this.router.navigateByUrl('/cart-page')
  }

}
